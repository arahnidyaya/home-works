package ru.ulanov.hw9.Animal;

import ru.ulanov.hw9.flyrun.Fly;
import ru.ulanov.hw9.flyrun.Run;
import ru.ulanov.hw9.flyrun.Swim;

public class Main  {


    public static void main(String[] args) {
        Animal lion = new Lion();
        Run run = new Lion();
        Swim swim2 = new Lion();
        Animal dolphin = new Dolphin();
        Animal eagle = new Eagle();
        Swim swim = new Dolphin();
        Fly fly = new Eagle();

        lion.getName();
        swim2.Swim();
        run.Run();
        dolphin.getName();
        swim.Swim();
        eagle.getName();
        fly.Fly();


    }
}
