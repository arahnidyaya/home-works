package ru.ulanov.hw9.Animal;

import ru.ulanov.hw9.flyrun.Run;
import ru.ulanov.hw9.flyrun.Swim;

public class Lion extends Animal implements Run, Swim {
    String name;
   public  void getName() {
        name = "Лев";
        System.out.println(name);

    }

    @Override
    public String Run() {
       String r = "я могу бегать";
        System.out.println(r);
        return r;
    }

    @Override
    public String Swim() {
        String s = "Я могу плавать";
        System.out.println(s);
        return s;
    }
}
