package ru.ulanov.hw8.agreeact;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;

public class Agreement {
    private int number;
    private String name;
    private String date;
    int i = 0;

    public int getNumber() {
        Scanner n = new Scanner(System.in);
        System.out.println("Введите номер договора: ");
        number = n.nextInt();
        return number;
    }

    public void setNumber() {

        this.number = number;
    }

    public String[] getName() {
        String[] name = {"товар1", "товар2", "товар3"};

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dt.parse(dt.format(new Date()));
//        System.out.println("Дата: " + nd);
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
