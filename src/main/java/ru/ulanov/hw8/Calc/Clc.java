package ru.ulanov.hw8.Calc;

import java.util.Scanner;

public class Clc {
    static Scanner scanner;

    public static int getInt() {

        scanner = new Scanner(System.in);

        System.out.println("Введите число:");
        int num;
        if (scanner.hasNextInt()) {
            num = scanner.nextInt();
        } else {
            System.out.println("Вы допустили ошибку при вводе числа. Попробуйте еще раз.");
            scanner.next();
            num = getInt();
        }

        return num;
    }

    public static char getOperation() {
        System.out.println("Введите операцию:");
        char operation;
        if (scanner.hasNext()) {
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Вы допустили ошибку при вводе операции. Попробуйте еще раз.");
            scanner.next();
            operation = getOperation();
        }

        return operation;
    }

    public static double calc(int num1, int num2, char operation) {
        double result;
        switch (operation) {
            case '*':
                result = num1 * num2;
                break;
            case '+':
                result = num1 + num2;
                break;
            case ',':
            case '.':
            default:
                System.out.println("Операция не распознана. Повторите ввод.");
                result = calc(num1, num2, getOperation());
                break;
            case '-':
                result = num1 - num2;
                break;
            case '/':
                result = num1 / num2;
        }

        return result;
    }


}

